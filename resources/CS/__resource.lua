-- server scripts
server_scripts{
  "deletepoliceweapons-server.lua",
  "me-server.lua",
  "ooc-server.lua",
  --"3dmser.lua",
  -- "deletevehicle-server.lua",
  "weathersync-server.lua",
  -- "fix-server.lua",
  "dispatch-server.lua"
}

-- client scripts
client_scripts{
  "anim-s.lua",
  "richpress.lua",
  "crouch-client.lua",
  'holograms.lua',
  'holo-config.lua',
  "pointfinger-client.lua",
  "handsup-client.lua",
  "deletepoliceweapons-client.lua",
  --"names-client.lua",
  -- "deletevehicle-client.lua",
  "weathersync-client.lua",
  -- "fix-client.lua",
  "missiontext-client.lua",
  "noradar.lua",
  --"3dmcli.lua",
  "discord.lua",
  "speed.lua",
  "xmas.lua",
  "norecticle.lua",
  "cruise.lua",
  "street-label.lua",
  "street-config.lua",
  "noweapondrops-client.lua"
 }

  exports {
    'getSurrenderStatus',
}
