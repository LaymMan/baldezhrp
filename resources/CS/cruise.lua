local enableCruise = false
Citizen.CreateThread( function()
  while true do
  Citizen.Wait( 0 )
  local ped = GetPlayerPed(-1)
  local vehicle = GetVehiclePedIsIn(ped, false)
  local vehicleModel = GetEntityModel(vehicle)
  local speed = GetEntitySpeed(vehicle)
  local Max = 1000.14
  local outVehicle = IsPedSittingInAnyVehicle(ped)
  if ( ped ) then
    if IsControlJustPressed(1, 183) then
      local inVehicle = IsPedSittingInAnyVehicle(ped)
      if (inVehicle) then
        if (GetPedInVehicleSeat(vehicle, - 1) == ped) then
          --vehicle = GetVehiclePedIsIn(ped, false)
          --speed = GetEntitySpeed(vehicle)
          if enableCruise == false then
            SetEntityMaxSpeed(vehicle, speed)
            --SetVehicleForwardSpeed(vehicle, speed)
            exports.pNotify:SendNotification({text = "[Круиз-контроль ON:] Максимальная скорость ".. math.floor(speed * 3.6).." km/h.", type = "success", timeout = 2500, layout = "centerRight"})
            enableCruise = true
          else

            SetEntityMaxSpeed(vehicle, Max)
            exports.pNotify:SendNotification({text = "[Круиз-контроль OFF:]", type = "success", timeout = 2500, layout = "centerRight"})
            enableCruise = false
          end
        end
      end
      if (outVehicle) == false then
        exports.pNotify:SendNotification({text = "[Круиз-контроль] Вы должны быть в машине", type = "error", timeout = 2500, layout = "centerRight"})
      end
    end
  end
end
end)
