
local lang = {
  repair = "Починить {1}.",
  reward = "Оплата: {1} $.",
  delivery = {
    title = "Доставка",
    item = "- {2} {1}"
  }
}

return lang
