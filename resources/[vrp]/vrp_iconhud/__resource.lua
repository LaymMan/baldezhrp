ui_page "ui.html"

files {
    "ui.html",
    "ui.css",
    "ui.js"
}

client_scripts{
    "@vrp/lib/utils.lua",
    "client.lua"
}

server_scripts {
    "@vrp/lib/utils.lua",
    "server.lua"
}