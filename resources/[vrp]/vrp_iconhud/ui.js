// Credits: Marmota#2533
$(document).ready(function(){
    window.addEventListener('message', function(event) {
        var data = event.data;
        $(".container-fluid").css("display",data.show? "none":"block");
        $("#armor").css("width",data.armor + "%");
        $('svg .circle-lifes').attr("stroke-dasharray",data.health+' 100');
        $('svg .circle-hunger').attr("stroke-dasharray",100 - data.hunger+' 100');
        $('svg .circle-thirst').attr("stroke-dasharray",100 - data.thirst+' 100');
    });
});
