--[[
Все новые клиентские функции объявляем тут!
Обязательно приставка -- tvRP.FunctionName
]]

RegisterNetEvent('evac:car')
AddEventHandler('evac:car', function()
  	local playerped = GetPlayerPed(-1)
  	local coordfirst = GetEntityCoords(playerped, 1)
  	local coordsecond = GetOffsetFromEntityInWorldCoords(playerped, 0.0, 5.0, 0.0)
  	local veh = getVehFromCoords(coordfirst, coordsecond)
  	if IsEntityAVehicle(veh) then
  	local pedveh = CreatePedInsideVehicle(veh,26,1644266841,-1,true,true)
  	SetVehicleEngineOn(veh,true,true,true)
  	TaskVehicleDriveToCoord(pedveh,veh,404.30883789062,-1633.4680175782,29.291931152344,80.0,1.0,0,1074528293,1.0)
  	Citizen.Wait(1000)
  	while DoesEntityExist(veh) do
    	Citizen.Wait(1000)
    	SetModelAsNoLongerNeeded(veh)
    	SetEntityAsNoLongerNeeded(veh)
    	SetEntityAsMissionEntity(veh, false, false)
    	DeleteEntity(veh)
    	SetModelAsNoLongerNeeded(pedveh)
    	SetEntityAsNoLongerNeeded(pedveh)
    	SetEntityAsMissionEntity(pedveh, false, false)
    	DeleteEntity(pedveh)
  	end
	exports.pNotify:SendNotification({text = "Транспорт эвакуирован", type = "error", timeout = 2500, layout = "centerRight", queue = "right"})
	else
	exports.pNotify:SendNotification({text = "Нет транспорта", type = "error", timeout = 2500, layout = "centerRight", queue = "right"})
	end
end)

function getVehFromCoords(coord1, coord2)
	local handle = CastRayPointToPoint(coord1.x, coord1.y, coord1.z, coord2.x, coord2.y, coord2.z, 10, GetPlayerPed(-1), 0)
	local a, b, c, d, vehicle = GetRaycastResult(handle)
	return vehicle
end

function tvRP.getVehicleInDirection(coordFrom, coordTo)
  local rayHandle = CastRayPointToPoint(coordFrom.x, coordFrom.y, coordFrom.z, coordTo.x, coordTo.y, coordTo.z, 10, GetPlayerPed( -1 ), 0)
  local _, _, _, _, vehicle = GetRaycastResult(rayHandle)
  return vehicle
end

function tvRP.deleteVehicleInFrontOrInside(offset)
  local ped = GetPlayerPed(-1)
  local veh = nil
  if (IsPedSittingInAnyVehicle(ped)) then
    veh = GetVehiclePedIsIn(ped, false)
  else
    veh = tvRP.getVehicleInDirection(GetEntityCoords(ped, 1), GetOffsetFromEntityInWorldCoords(ped, 0.0, offset, 0.0))
  end

  if IsEntityAVehicle(veh) then
    SetVehicleHasBeenOwnedByPlayer(veh, false)
    Citizen.InvokeNative(0xAD738C3085FE7E11, veh, false, true)
    SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(veh))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(veh))
    exports.pNotify:SendNotification({text = "Транспорт удалён", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
  else
    exports.pNotify:SendNotification({text = "Слишком далеко от транспорта", type = "error", timeout = 2500, layout = "centerRight", queue = "right"})
  end
end

function tvRP.setArmour(armour, vest) -- устанавливает значения армора педу
  local player = GetPlayerPed(-1)
  if vest then
    if(GetEntityModel(player) == GetHashKey("mp_m_freemode_01")) then
      SetPedComponentVariation(player, 9, 4, 1, 2)
    else
      if(GetEntityModel(player) == GetHashKey("mp_f_freemode_01")) then
        SetPedComponentVariation(player, 9, 6, 1, 2)
      end
    end
  end
  local n = math.floor(armour)
  SetPedArmour(player, n)
end

function tvRP.unsetArmour() --
local player = GetPlayerPed(-1)
  SetPedComponentVariation(player, 9, 6, 1, 2)
end

function tvRP.drawsTattoo(tattoo,tattooshop)
  ApplyPedOverlay(GetPlayerPed(-1), GetHashKey(tattooshop), GetHashKey(tattoo))
end

function tvRP.deleteNearestVehicle(radius) -- удаляет тс в радиусе
  local x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
  local veh = vRPbm.getNearestVehicle(radius)

  if IsEntityAVehicle(veh) then
    SetVehicleHasBeenOwnedByPlayer(veh, false)
    Citizen.InvokeNative(0xAD738C3085FE7E11, veh, false, true)
    SetVehicleAsNoLongerNeeded(Citizen.PointerValueIntInitialized(veh))
    Citizen.InvokeNative(0xEA386986E786A54F, Citizen.PointerValueIntInitialized(veh))
    exports.pNotify:SendNotification({text = "Транспорт удалён", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
  else
    exports.pNotify:SendNotification({text = "Слишком далеко от транспорта", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
  end
end

function tvRP.getNearestVehicle(radius) --  возвращает значение ближайшего транспорта в радиусе около педа
  local x, y, z = table.unpack(GetEntityCoords(GetPlayerPed(-1), true))
  local ped = GetPlayerPed(-1)
  if IsPedSittingInAnyVehicle(ped) then
    return GetVehiclePedIsIn(ped, true)
  else
    local veh = GetClosestVehicle(x + 0.0001, y + 0.0001, z + 0.0001, radius + 5.0001, 0, 8192 + 4096 + 4 + 2 + 1) -- лодки или самолёты
    if not IsEntityAVehicle(veh) then veh = GetClosestVehicle(x + 0.0001, y + 0.0001, z + 0.0001, radius + 5.0001, 0, 4 + 2 + 1) end -- машины
    return veh
  end
end

function tvRP.getArmour() -- возвращает значение аромра у педа
  return GetPedArmour(GetPlayerPed(-1))
end

function tvRP.spawnVehicle(model) -- спавнит машину
  local i = 0
  local mhash = GetHashKey(model)
  while not HasModelLoaded(mhash) and i < 1000 do
    if math.fmod(i, 100) == 0 then
      exports.pNotify:SendNotification({text = "Загрузка модели", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
    end
    RequestModel(mhash)
    Citizen.Wait(30)
    i = i + 1
  end

  if HasModelLoaded(mhash) then
    local x, y, z = vRP.getPosition({})
    local nveh = CreateVehicle(mhash, x, y, z + 0.5, GetEntityHeading(GetPlayerPed(-1)), true, false)
    SetVehicleOnGroundProperly(nveh)
    SetEntityInvincible(nveh, false)
    SetPedIntoVehicle(GetPlayerPed(-1), nveh, - 1)
    Citizen.InvokeNative(0xAD738C3085FE7E11, nveh, true, true)
    SetVehicleHasBeenOwnedByPlayer(nveh, true)
    SetModelAsNoLongerNeeded(mhash)
    exports.pNotify:SendNotification({text = "Транспорт заспавнен", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
  else
    exports.pNotify:SendNotification({text = "Неправильное имя модели", type = "error", timeout = 2500, layout = "centerRight", queue = "right"})
  end
end

function tvRP.lockpickVehicle(wait, any) -- взлом машины
  Citizen.CreateThread(function()
    local pos = GetEntityCoords(GetPlayerPed(-1))
    local entityWorld = GetOffsetFromEntityInWorldCoords(GetPlayerPed(-1), 0.0, 20.0, 0.0)

    local rayHandle = CastRayPointToPoint(pos.x, pos.y, pos.z, entityWorld.x, entityWorld.y, entityWorld.z, 10, GetPlayerPed(-1), 0)
    local _, _, _, _, vehicleHandle = GetRaycastResult(rayHandle)
    if DoesEntityExist(vehicleHandle) then
      if GetVehicleDoorsLockedForPlayer(vehicleHandle, PlayerId()) or any then
        local prevObj = GetClosestObjectOfType(pos.x, pos.y, pos.z, 10.0, GetHashKey("prop_weld_torch"), false, true, true)
        if(IsEntityAnObject(prevObj)) then
          SetEntityAsMissionEntity(prevObj)
          DeleteObject(prevObj)
        end
        StartVehicleAlarm(vehicleHandle)
        TaskStartScenarioInPlace(GetPlayerPed(-1), "WORLD_HUMAN_WELDING", 0, true)
        Citizen.Wait(wait * 1000)
        SetVehicleDoorsLocked(vehicleHandle, 1)
        for i = 1, 64 do
          SetVehicleDoorsLockedForPlayer(vehicleHandle, GetPlayerFromServerId(i), false)
        end
        ClearPedTasksImmediately(GetPlayerPed(-1))

        exports.pNotify:SendNotification({text = "[Транспорт открыт]", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})

        local plate = GetVehicleNumberPlateText(vehicleHandle)
        HKserver.lockSystemUpdate({1, plate})
        HKserver.playSoundWithinDistanceOfEntityForEveryone({vehicleHandle, 10, "unlock", 1.0})
      else
        exports.pNotify:SendNotification({text = "[Транспорт уже открыт]", timeout = 2500, layout = "centerRight", queue = "right"})
      end
    else
      exports.pNotify:SendNotification({text = "Далеко от транспорта", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
    end
  end)
end

function tvRP.loadFreeze(notify, god, ghost) --  фризит игрока
  if not frozen then
    if notify then
      exports.pNotify:SendNotification({text = "Вы были заморожены", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
    end
    frozen = true
    invincible = god
    invisible = ghost
    unfrozen = false
  else
    if notify then
      exports.pNotify:SendNotification({text = "Вы были разморожены", type = "success", timeout = 2500, layout = "centerRight", queue = "right"})
    end
    unfrozen = true
    invincible = false
    invisible = false
  end
end
