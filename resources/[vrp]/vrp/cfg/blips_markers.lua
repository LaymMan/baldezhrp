
-- this file is used to define additional static blips and markers to the map
-- some lists: https://wiki.gtanet.work/index.php?title=Blips

local cfg = {}

-- list of blips
-- {x,y,z,idtype,idcolor,text}
cfg.blips = {
  {-1202.96252441406,-1566.14086914063,4.61040639877319,311,17,"Тренажерный зал"},
  {446.59762573242,-984.01184082032,30.68957901001,60,38,"Полиция"},
  {-467.96713256836,-337.70986938476,34.371070861816,61,1,"Больница"},
  {479.2579650879,-1316.1966552734,29.203424453736,446,5,"СТО"},
  {907.5234375,-177.5662536621,74.123146057128,198,46,"Такси"}
}

-- list of markers
-- {x,y,z,sx,sy,sz,r,g,b,a,visible_distance}
cfg.markers = {
}

return cfg
