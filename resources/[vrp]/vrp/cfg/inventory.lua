
local cfg = {}

cfg.inventory_weight_per_strength = 10 -- weight for an user inventory per strength level (no unit, but thinking in "kg" is a good norm)

-- default chest weight for vehicle trunks
cfg.default_vehicle_chest_weight = 50

-- define vehicle chest weight by model in lower case
cfg.vehicle_chest_weights = {
  ["monster"] = 250
}

-- list of static chest types (map of name => {.title,.blipid,.blipcolor,.weight, .permissions (optional)})
cfg.static_chest_types = {
  ["Шкаф"] = { -- example of a static chest
    title = "Оружейный шкаф",
    weight = 100
  },
  ["Оружейная ballas"] = {
    title = "Оружейная ballas",
    weight = 100,
    permissions = {"ballas.chest"},
  },
  ["Оружейная groove"] = {
    title = "Оружейная Groove",
    weight = 100,
    permissions = {"groove.chest"},
  }
}

-- list of static chest points
cfg.static_chests = {
  {"Шкаф", 455.93572998046,-979.5234375,30.689580917358},
  {"Оружейная ballas", 118.2218170166,-1950.8790283204,20.74698638916},
  {"Оружейная groove", -17.887496948242,-1438.7908935546,31.101552963256}
}

return cfg
