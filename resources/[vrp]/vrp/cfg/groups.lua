
local cfg = {}

-- define each group with a set of permissions
-- _config property:
--- title (optional): group display name
--- gtype (optional): used to have only one group with the same gtype per player (example: a job gtype to only have one job)
--- onspawn (optional): function(player) (called when the player spawn with the group)
--- onjoin (optional): function(player) (called when the player join the group)
--- onleave (optional): function(player) (called when the player leave the group)
--- (you have direct access to vRP and vRPclient, the tunnel to client, in the config callbacks)

cfg.groups = {
  ["superadmin"] = {
    _config = {onspawn = function(player) vRPclient._notify(player,"You are superadmin.") end},
    "player.group.add",
    "player.group.remove",
    "player.givemoney",
    "player.giveitem",
    "player.store_money"
  },
  ["admin"] = {
    "admin.tickets",
    "admin.announce",
    "admin.deleteveh",
    "player.list",
    "player.whitelist",
    "player.unwhitelist",
    "player.kick",
    "player.ban",
    "player.unban",
    "player.noclip",
    "player.custom_emote",
    "player.custom_sound",
    "player.display_custom",
    "player.coords",
    "player.tptome",
    "player.tpto",
    "admin.blips",
    "admin.crun",
    "admin.deleteveh",
    "freeze.admin",
    "admin.godmode",
    "admin.spawnveh",
    "admin.sprites",
    "admin.srun",
    "admin.tptowaypoint"

  },
  ["god"] = {
    "admin.god" -- reset survivals/health periodically
  },
  -- the group user is auto added to all logged players
  ["user"] = {
    "player.phone",
    "player.store_money",
    "player.loot",
    "player.calladmin",
    "police.askid",
    "police.store_weapons",
    "police.seizable" -- can be seized
  },
  ["police"] = {
    _config = {
      title = "Полиция",
      gtype = "job",
      onjoin = function(player) vRPclient._setCop(player,true) end,
      onspawn = function(player) vRPclient._setCop(player,true) end,
      onleave = function(player) vRPclient._setCop(player,false) end
    },
    "police.menu",
    "car.police",
    "police.cloakroom",
    "police.pc",
    "police.handcuff",
    "police.drag",
    "police.putinveh",
    "police.getoutveh",
    "police.check",
    "police.service",
    "police.wanted",
    "police.seize.weapons",
    "police.seize.items",
    "police.jail",
    "police.fine",
    "police.announce",
    "-police.store_weapons",
    "-police.seizable",
    "emergency.revive" -- negative permission, police can't seize itself, even if another group add the permission
  },
  ["emergency"] = {
    _config = {
      title = "Больница",
      gtype = "job"
    },
    "emergency.revive",
    "emergency.shop",
    "emergency.service"
  },
  ["ballas"] = {
    _config = { gtype = "job",
	},
  "car.ballas",
  "ballas.loadshop",
  "ballas.chest",
  "police.menu",
  "police.handcuff",
  "police.drag",
  "police.putinveh",
  "police.getoutveh",
  },
  ["groove"] = {
    _config = { gtype = "job",
  },
  "car.groove",
  "groove.loadshop",
  "groove.chest",
  "police.menu",
  "police.handcuff",
  "police.drag",
  "police.putinveh",
  "police.getoutveh",
 },
  ["repair"] = {
    _config = {
      title = "Механик",
      gtype = "job"
    },
    "vehicle.repair",
    "vehicle.replace",
    "repair.service",
    "mission.repair.satellite_dishes"
  },
  ["delivery"] = {
    _config = {
      title = "Доставка",
      gtype = "job"
    },
    "mission.delivery.food"
  },
  ["taxi"] = {
    _config = {
      title = "Такси",
      gtype = "job"
    },
    "taxi.service",
    "car.taxi"
  },
  ["citizen"] = {
    _config = {
      title = "Безработный",
      gtype = "job"
    }
  }
}

-- groups are added dynamically using the API or the menu, but you can add group when an user join here
cfg.users = {
  [1] = { -- give superadmin and admin group to the first created user on the database
    "superadmin",
    "admin"
  }
}

-- group selectors
-- _config
--- x,y,z, blipid, blipcolor, permissions (optional)

cfg.selectors = {
  ["Биржа труда"] = {
    _config = {x = -268.363739013672, y = -957.255126953125, z = 31.22313880920410, blipid = 351, blipcolor = 47},
    "taxi",
    "delivery",
    "citizen"
  }
}

return cfg
