
-- define all language properties

local lang = {
  common = {
    welcome = "Добро пожаловать! Нажмите ~Ё~ чтоб открыть меню.", -- Welcome. Use the phone keys to use the menu.~n~last login: {1}
    no_player_near = "~r~Возле вас нет игроков.",
    invalid_value = "~r~Недопустимое значение.",
    invalid_name = "~r~Недопустимое имя.",
    not_found = "~r~Не найдено.",
    request_refused = "~r~Запрос отклонен.",
    wearing_uniform = "~r~Осторожно! На вас униформа.",
    not_allowed = "~r~Не положено."
  },
  weapon = {
    pistol = "Пистолет"
  },
  survival = {
    starving = "Голод",
    thirsty = "Жажда"
  },
  money = {
    display = "{1} <span class=\"symbol\">$</span>",
    given = "Вы передали ~r~{1}$.",
    received = "Вы получили ~g~{1}$.",
    not_enough = "~r~Не достаточно денег.",
    paid = "Оплачено ~r~{1}$.",
    give = {
      title = "Передать деньги",
      description = "Дать деньги ближайшему человеку",
      prompt = "Количество для передачи:"
    }
  },
  inventory = {
    title = "Инвентарь",
    description = "Открыть инвентарь.",
    iteminfo = "({1})<br /><br />{2}<br /><em>{3} кг</em>",
    info_weight = "вес {1}/{2} кг",
    give = {
      title = "Передать",
      description = "Передать вещи ближайшему игроку",
      prompt = "Сколько передать (max {1}):",
      given = "Вы передали ~r~{1} ~s~{2}.",
      received = "Вы получили ~g~{1} ~s~{2}.",
    },
    trash = {
      title = "Выбросить",
      description = "Выбросить предметы",
      prompt = "Сколько выбросить (max {1}):",
      done = "Вы выбросили ~r~{1} ~s~{2}."
    },
    missing = "~r~Отсутствует {2} {1}.",
    full = "~r~Инвентарь полон",
    chest = {
      title = "Хранилище",
      already_opened = "~r~Уже кем-то открыто",
      full = "~r~Хранилище переполнено.",
      take = {
        title = "Взять",
        prompt = "Сколько взять (max {1}):"
      },
      put = {
        title = "Положить",
        prompt = "Сколько положить (max {1}):"
      }
    }
  },
  atm = {
    title = "Банкомат",
    info = {
      title = "Информация",
      bank = "Счёт: {1} $"
    },
    deposit = {
      title = "Положить в банк",
      description = "Положить наличные в банк",
      prompt = "Количество денег для депозита:",
      deposited = "~r~{1}$~s~ были переведены на ваш счёт."
    },
    withdraw = {
      title = "Снять наличные",
      description = "Снять деньги с банковского счёта",
      prompt = "Сколько денег снять:",
      withdrawn = "~g~{1}$ ~s~ обналичены.",
      not_enough = "~r~У вас нет столько денег в банке."
    }
  },
  business = {
    title = "Торговая палата",
    directory = {
      title = "Каталог",
      description = "Бизнес каталог.",
      dprev = "> Пред.",
      dnext = "> След.",
      info = "<em>капитал: </em>{1} $<br /><em>владелец: </em>{2} {3}<br /><em>регистрационный номер: </em>{4}<br /><em>телефон: </em>{5}"
    },
    info = {
      title = "Информация о бизнесе",
      info = "<em>Имя: </em>{1}<br /><em>капитал: </em>{2} $<br /><em>перевод капитала: </em>{3} $<br /><br/>Передача капитала - это сумма денег, переведенная за экономический период бизнеса, максимальная сумма - бизнес-капитал."
    },
    addcapital = {
      title = "Внести капитал",
      description = "Внести капитал в вашу компанию.",
      prompt = "Количество для внесения:",
      added = "~r~{1}$ ~s~было начислено на ваш бизнес-счёт."
    },
    launder = {
      title = "Отмывание денег",
      description = "Используйте свой бизнес, чтоб отмыть грязные деньги.",
      prompt = "Сумма для отмывки (макс {1} $): ",
      laundered = "~g~{1}$ ~s~отмыто.",
      not_enough = "~r~Недостаточно грязных денег."
    },
    open = {
      title = "Открыть бизнес",
      description = "Открыть бизнес можно имея минимальный капитал {1} $.",
      prompt_name = "Название вашей компании ({1} макс симв. ):",
      prompt_capital = "Начальный капитал (мин {1})",
      created = "~g~Бизнес создан."

    }
  },
  cityhall = {
    title = "Мэрия",
    identity = {
      title = "Новый паспорт",
      description = "Купить новый паспорт, цена = {1} $.",
      prompt_firstname = "Введите вашу фамилию:",
      prompt_name = "Введите ваше имя:",
      prompt_age = "Введите ваш возраст:",
    },
    menu = {
      title = "Паспорт",
      info = "<em>Фамилия: </em>{1}<br /><em>Имя: </em>{2}<br /><em>Возраст: </em>{3}<br /><em>регистрационный номер: </em>{4}<br /><em>Телефон: </em>{5}<br /><em>Адрес: </em>{7}, {6}"
    }
  },
  police = {
    title = "Полиция",
    wanted = "Розыск : КОД {1}",
    not_handcuffed = "~r~Не в наручниках",
    cloakroom = {
      title = "Раздевалка",
      uniform = {
        title = "Униформа",
        description = "Одеть униформу."
      }
    },
    pc = {
      title = "Компьютер",
      searchreg = {
        title = "Поиск по ID",
        description = "Поиск человека по паспорту.",
        prompt = "Введите ID:"
      },
      closebusiness = {
        title = "Закрыть бизнес",
        description = "Закрыть бизнес ближайшего игрока.",
        request = "Вы уверены, что хотите закрыть бизнес {3} на имя {1} {2} ?",
        closed = "~g~Бизнес закрыт."
      },
      trackveh = {
        title = "Отследить авто",
        description = "Отследить авто по номеру.",
        prompt_reg = "Введите ID:",
        prompt_note = "Напишите причину отслеживания:",
        tracking = "~b~Слежение установлено.",
        track_failed = "~b~Слежение за {1}~s~ ({2}) ~n~~r~ не удалось установить.",
        tracked = "Отслеживать {1} ({2})"
      },
      records = {
        show = {
          title = "Посмотреть записи",
          description = "Посмотреть записи полиции по ID."
        },
        delete = {
          title = "Очистить записи",
          description = "Очистить записи полиции по ID",
          deleted = "~b~База данных была очищена"
        }
      }
    },
    menu = {
      handcuff = {
        title = "Наручники",
        description = "Одеть/Снять наручники с ближайшего игрока."
      },
      drag = {
        title = "Сопровождение",
        description = "Заставить ближайшего игрока в наручниках следовать за вами."
      },
      putinveh = {
        title = "Посадить в автомобиль",
        description = "Помещает ближайшего игрока в наручниках в автомобиль."
      },
      getoutveh = {
        title = "Вытащить из автомобиля",
        description = "Вытащить ближайшего игрока в наручниках из автомобиля."
      },
      askid = {
        title = "Спросить паспорт",
        description = "Попросить посмотреть ID у ближайшего игрока.",
        request = "Вы хотите показать свой паспорт?",
        request_hide = "Вернуть паспорт.",
        asked = "Ожидаем ID..."
      },
      check = {
        title = "Обыскать",
        description = "Обыскать ближайшего игрока.",
        request_hide = "Скрыть отчет о проверке.",
        info = "<em>деньги: </em>{1} $<br /><br /><em>снаряжение: </em>{2}<br /><br /><em>оружие: </em>{3}",
        checked = "Вы были досмотрены."
      },
      seize = {
        seized = "Изъто {2} ~r~{1}",
        weapons = {
          title = "Изъять оружие",
          description = "Изъять оружие",
          seized = "~b~Ваше оружие было изъято."
        },
        items = {
          title = "Изъять нелегал",
          description = "Изымает нелегальные вещи.",
          seized = "~b~Ваши нелегальные вещи были изъяты."
        }
      },
      jail = {
        title = "Посадить",
        description = "Посадить/Вытащить ближайшего игрока из тюрьмы",
        not_found = "~r~Рядом нет тюрьмы.",
        jailed = "~b~Посажен.",
        unjailed = "~b~Выпущен.",
        notify_jailed = "~b~Вы были посажены.",
        notify_unjailed = "~b~Вас выпустили."
      },
      fine = {
        title = "Штраф",
        description = "Оштрафовать ближайшего игрока.",
        fined = "~b~Оштрафовал ~s~{2} $ за ~b~{1}.",
        notify_fined = "~b~Вы были оштрафованы ~s~ {2} $ за ~b~{1}.",
        record = "[Штраф] {2} $ за {1}"
      },
      store_weapons = {
        title = "Сложить оружие",
        description = "Сложить всё оружие в инвентарь."
      }
    },
    identity = {
      info = "<em>Фамилия: </em>{1}<br /><em>Имя: </em>{2}<br /><em>Возраст: </em>{3}<br /><em>Регистрационный номер: </em>{4}<br /><em>Телефон: </em>{5}<br /><em>Бизнес: </em>{6}<br /><em>Капитал бизнеса: </em>{7} $<br /><em>Адрес: </em>{9}, {8}"
    }
  },
  emergency = {
    menu = {
      revive = {
        title = "Первая помощь",
        description = "Оказать первую помощь ближайшему человеку.",
        not_in_coma = "~r~Не в коме."
      }
    }
  },
  phone = {
    title = "Телефон",
    directory = {
      title = "Контакты",
      description = "Открывает записную книгу",
      add = {
        title = "> Добавить",
        prompt_number = "Введите телефон для добавления:",
        prompt_name = "Введите имя контакта:",
        added = "~g~Контакт добавлен."
      },
      sendsms = {
        title = "Отправить сообщение",
        prompt = "Отправить сообщение (max {1} chars):",
        sent = "~g~ Отправить n°{1}.",
        not_sent = "~r~ n°{1} недоступен."
      },
      sendpos = {
        title = "Отправить позицию",
      },
      remove = {
        title = "Удалить"
      },
      call = {
        title = "Позвонить",
        not_reached = "~r~ n°{1} вне зоны доступа."
      }
    },
    sms = {
      title = "Сообщения",
      description = "Полученные сообщения",
      info = "<em>{1}</em><br /><br />{2}",
      notify = "SMS~b~ {1}:~s~ ~n~{2}"
    },
    smspos = {
      notify = "SMS-Position ~b~ {1}"
    },
    service = {
      title = "Службы",
      description = "Позвонить в службу экстренной помощи.",
      prompt = "Введите сообщение в диспетчерскую:",
      ask_call = "Получен {1} вызов, принять его? <em>{2}</em>",
      taken = "~r~Этот вызов уже обрабатывается."
    },
    announce = {
      title = "Оповещение",
      description = "Опубликовать объявление, которое будут видеть все.",
      item_desc = "{1} $<br /><br/>{2}",
      prompt = "Содержание объявления (10-1000 символов): "
    },
    call = {
      ask = "Принять звонок от {1} ?",
      notify_to = "Звонок~b~ {1}...",
      notify_from = "Принять звонок от ~b~ {1}...",
      notify_refused = "Звонок ~b~ {1}... ~r~ не удался."
    },
    hangup = {
      title = "Повесить трубку",
      description = "Повесить трубку (Завершает текущий звонок)"
    }
  },
  emotes = {
    title = "Эмоции",
    clear = {
      title = "> Остановить",
      description = "Остановить текущую эмоцию"
    }
  },
  home = {
    buy = {
      title = "Купить",
      description = "Купить дом за {1} $.",
      bought = "~g~Приобретено.",
      full = "~r~Дом уже приобретен кем-то другим.",
      have_home = "~r~У Вас уже есть дом."
    },
    sell = {
      title = "Продать",
      description = "Продать Ваш дом за {1} $",
      sold = "~g~Продано.",
      no_home = "~r~У Вас нет тут имущества."
    },
    intercom = {
      title = "Домофон",
      description = "Используйте домофон чтоб войти.",
      prompt = "Номер:",
      not_available = "~r~Не доступно.",
      refused = "~r~Отказано во входе.",
      prompt_who = "Кто там? :",
      asked = "Звоним...",
      request = "Кто-то стучит в вашу дверь: <em>{1}</em>"
    },
    slot = {
      leave = {
        title = "Выйти"
      },
      ejectall = {
        title = "Выгнать всех",
        description = "Выгнать всех включая Вас и закрыть дом"
      }
    },
    wardrobe = {
      title = "Гардероб",
      save = {
        title = "> Сохранить",
        prompt = "Название:"
      }
    },
    gametable = {
      title = "Кости",
      bet = {
        title = "Сделать ставку",
        description = "Начать  игру с игроками рядом, победитель выбирается случайно",
        prompt = "Цена ставки:",
        request = "[BET] Вы хотите поставить {1} $ ?",
        started = "~g~Ставка сделана."
      }
    },
    radio = {
      title = "Радио",
      off = {
        title = "> Выключить"
      }
    }
  },
  garage = {
    title = "Гараж {1}",  --------------------------- ({1})
    owned = {
      title = "Ваша техника",
      description = "Ваша техника."
    },
    buy = {
      title = "Купить",
      description = "Купить автомобиль.",
      info = "{1} $<br /><br />{2}"
    },
    sell = {
      title = "Продать",
      description = "Продать автомобиль"
    },
    rent = {
      title = "Арендовать",
      description = "Арендовать автомобиль (До момента выхода с сервера)"
    },
    store = {
      title = "Поставить в гараж",
      description = "Загнать технику в гараж.",
      too_far = "Техника слишком далеко",
      wrong_garage = "Нельзя загнать автомобиль в этот гараж"
    }
  },
  vehicle = {
    title = "Автомобиль",
    no_owned_near = "~r~Рядом нет вашей техники.",
    trunk = {
      title = "Багажник",
      description = "Открыть багажник"
    },
    detach_trailer = {
      title = "Отцепить прицеп",
      description = "Отцепить прицеп"
    },
    detach_towtruck = {
      title = "Отцепить эвакуатор",
      description = "Отцепить эвакуатор"
    },
    detach_cargobob = {
      title = "Отцепить каргобоб",
      description = "Отцепить каргобоб"
    },
    lock = {
      title = "Закрыть/Открыть",
      description = "Закрыть/Открыть автомобиль"
    },
    engine = {
      title = "Вкл/Выкл двигатель",
      description = "Завести/заглушить автомобиль"
    },
    asktrunk = {
      title = "Попросить открыть багажник",
      asked = "~g~Спрашиваем...",
      request = "Вы хотите открыть багажник?"
    },
    replace = {
      title = "Переместить автомобиль",
      description = "Переместить на землю ближайший автомобиль."
    },
    repair = {
      title = "Ремонт автомобиля",
      description = "Отремонтировать ближайший автомобиль."
    }
  },
  gunshop = {
    title = "Оружейная ({1})",
    prompt_ammo = "Количество патронов для {1}:",
    info = "<em>оружие: </em> {1} $<br /><em>патроны: </em> {2} $/u<br /><br />{3}"
  },
  market = {
    title = "Магазин ({1})",
    prompt = "Количество {1} для покупки:",
    info = "{1} $<br /><br />{2}"
  },
  skinshop = {
    title = "Магазин одежды"
  },
  cloakroom = {
    title = "Раздевалка ({1})",
    undress = {
      title = "> Снять"
    }
  },
  itemtr = {
    not_enough_reagents = "~r~Недостаточно реагентов",
    informer = {
      title = "Информатор",
      description = "{1} $",
      bought = "~g~Локация была отправлена на ваш GPS."
    }
  },
  mission = {
    blip = "Задание ({1}) {2}/{3}",
    display = "<span class=\"name\">{1}</span> <span class=\"step\">{2}/{3}</span><br /><br />{4}",
    cancel = {
      title = "Отменить задание"
    }
  },
  aptitude = {
    title = "Способности",
    description = "Посмотреть характеристики персонажа",
    lose_exp = "Вы потеряли ~b~{1}/{2} ~r~-{3} ~s~опыт.",
    earn_exp = "Вы получили ~b~{1}/{2} ~g~+{3} ~s~опыт.",
    level_down = "Способность ~b~{1}/{2} ~r~потеряли уровень. ({3}).",
    level_up = "Способность ~b~{1}/{2} ~g~повысили уровень ({3}).",
    display = {
      group = "{1}: ",
      aptitude = "{1} LVL {3} EXP {2}"
    }
  },
  radio = {
    title = "Вкл/Выкл Радио"
  }
}

return lang
