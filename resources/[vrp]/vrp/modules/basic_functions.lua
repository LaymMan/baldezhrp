--[[
Объявлять ТОЛЬКО так:
function vRP.FunctionName ()
  ...
end
]]
--ФУНКЦИИ
function vRP.barman (player, ingrd, ingrd2, food, name, amount1, amount2, amountf) -- коктейли
  local seq = { {"amb@prop_human_bbq@male@base", "base", 1} }
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    if vRP.getInventoryItemAmount(user_id, ingrd) > 0 and vRP.getInventoryItemAmount(user_id, ingrd2) > 0 then
      vRP.tryGetInventoryItem(user_id, ingrd, amount1)
      vRP.tryGetInventoryItem(user_id, ingrd2, amount2)
      vRP.giveInventoryItem(user_id, food, amountf)
      vRPclient.playAnim(player, {true, seq, false})
      TriggerClientEvent("pNotify:SendNotification", user_id, {text = "Коктейль " ..name, type = "success", timeout = (3000), layout = "centerRight"})
    else
      TriggerClientEvent("pNotify:SendNotification", user_id, {text = "У вас нету нужных ингредиентов", type = "error", timeout = (3000), layout = "centerRight"})
    end
  end
end

function vRP.cook (player, ingrd, ingrd2, food, name, amount1, amount2, amountf) --готовка
  local seq = { {"amb@prop_human_bbq@male@base", "base", 1} }
  local user_id = vRP.getUserId(player)
  if user_id then
    if vRP.getInventoryItemAmount(user_id, ingrd) > 0 and vRP.getInventoryItemAmount(user_id, ingrd2) > 0 then
      vRP.tryGetInventoryItem(user_id, ingrd, amount1, false)
      vRP.tryGetInventoryItem(user_id, ingrd2, amount2, false)
      vRP.giveInventoryItem(user_id, food, amountf)
      vRPclient.playAnim(player, true, seq, false)
      TriggerClientEvent("pNotify:SendNotification", player, {text = "Вы приготовили " ..name, type = "success", timeout = (3000), layout = "centerRight"})
    else
      TriggerClientEvent("pNotify:SendNotification", player, {text = "У вас нету нужных ингредиентов", type = "error", timeout = (3000), layout = "centerRight"})
    end
  end
end




function vRP.SetPlayerJob (player) -- устроить на работу(ПД)
  local user_id = vRP.getUserId(player)
  if user_id ~= nil then
    vRP.prompt(player, "Номер паспорта:(игровой ID, не регистрационный) ", "", function(player, id)
      id = parseInt(id)
      local source = vRP.getUserSource(id)
      if source ~= nil then
        vRP.prompt(player, "Должность: ", "Police Officer", function(player, group)
          group = group or ""
          if group ~= "" and group ~= nil then
            if group == "Police Officer" then
              vRP.addUserGroup(id,group)
              TriggerClientEvent("pNotify:SendNotification", user_id, {text = "Принят на работу", type = "success", timeout = (3000), layout = "centerRight"})
            else
              TriggerClientEvent("pNotify:SendNotification", user_id, {text = "Такой должности нету", type = "error", timeout = (3000), layout = "centerRight"})
            end
          else
            TriggerClientEvent("pNotify:SendNotification", user_id, {text = "Пустая строка", type = "error", timeout = (3000), layout = "centerRight"})
          end
        end)
      end
    end)
  end
end
