  --[[
Все новые менюшные выборы(главное меню)
прописываем тут.
Функции для них объявляем либо в:
client/client_functions.lua
либо
modules/basic_functions.lua
!!Объвлять в тех файлах согласно правилам прописаным там.
]]


local function choice_loot (player, choice)
  local user_id = vRP.getUserId(player)
  local nplayer = vRPclient.getNearestPlayer(player, 10)
  local nuser_id = vRP.getUserId(nplayer)
  if user_id then
      if nuser_id then
          if vRPclient.isInComa(nplayer) then
            local revive_seq = {
              {"amb@medic@standing@kneel@enter", "enter", 1},
              {"amb@medic@standing@kneel@idle_a", "idle_a", 1},
              {"amb@medic@standing@kneel@exit", "exit", 1}
            }
             vRPclient._playAnim(player, false, revive_seq, false)
              SetTimeout(15000, function()
              local ndata = vRP.getUserDataTable(nuser_id)
              if ndata ~= nil then
                if ndata.inventory ~= nil then
                  local curr_weg = vRP.getInventoryMaxWeight(user_id) - vRP.getInventoryWeight(user_id)
                  if vRP.getInventoryWeight(nuser_id) > curr_weg then
                    TriggerClientEvent("pNotify:SendNotification", player, {text = "У вас полный инвентарь", type = "info", timeout = (3000), layout = "centerRight"})
                  else
                    for k, v in pairs(ndata.inventory) do
                      vRP.giveInventoryItem(user_id, k, v.amount, true)
                    end
                    vRP.clearInventory(nuser_id)
                  end
                end
              end
              local nmoney = vRP.getMoney(nuser_id)
              if vRP.tryPayment(nuser_id, nmoney) then
                vRP.giveMoney(user_id, nmoney)
              end
            end)
            vRPclient.stopAnim(player, false)
          else
            TriggerClientEvent("pNotify:SendNotification", player, {text = "Не в коме", type = "info", timeout = (3000), layout = "centerRight"})
          end
      else
        TriggerClientEvent("pNotify:SendNotification", player, {text = "Никого нету вблизи", type = "info", timeout = (3000), layout = "centerRight"})
    end
  end
end

local function choice_tptowaypoint (player, choice)
TriggerClientEvent("TpToWaypoint", player)
end

local function choice_store_money (player, choice)
local user_id = vRP.getUserId(player)
if user_id then
  local amount = vRP.getMoney(user_id)
  if amount > 0 then
    if vRP.tryPayment(user_id, amount) then
      vRP.giveInventoryItem(user_id, "money", amount, true)
    end
  else
    TriggerClientEvent("pNotify:SendNotification", player, {text = "У вас нет денег", type = "info", timeout = (3000), layout = "centerRight"})
  end
end
end

local function ch_fixhair (player, choice)
local custom = {}
local user_id = vRP.getUserId(player)
vRP.getUData(user_id, "vRP:head:overlay", function(value)
  if value ~= nil then
  custom = json.decode(value)
  vRPclient.setOverlay(player, {custom, true})
end
end)
end

local function ch_deleteveh (player, choice) -- Удалить машину
vRPclient.deleteVehicleInFrontOrInside(player, {5.0})
end

vRP.registerMenuBuilder("admin", function(add, data)
local user_id = vRP.getUserId(data.player)
if user_id then
local choices = {}

if vRP.hasPermission(user_id, "admin.deleteveh") then
  choices["Удалить транспорт"] = {ch_deleteveh} -- Delete nearest vehicle (Fixed pull request https://github.com/Sighmir/vrp_basic_menu/pull/11/files/419405349ca0ad2a215df90cfcf656e7aa0f5e9c from benjatw)
end

if vRP.hasPermission(user_id, "admin.deleteveh") then
  choices["Тп на точку"] = {choice_tptowaypoint} -- Delete nearest vehicle (Fixed pull request https://github.com/Sighmir/vrp_basic_menu/pull/11/files/419405349ca0ad2a215df90cfcf656e7aa0f5e9c from benjatw)
end

add(choices)
end
end)

vRP.registerMenuBuilder("main", function(add, data)
local user_id = vRP.getUserId(data.player)
if user_id then
local choices = {}
--Меню игрока
choices["Игрок"] = {function(player, choice)
  local menu = vRP.buildMenu("player_menu", {player = player})
  menu.name = "Игрок"
  menu.css = {top = "75px", header_color = "rgba(200,0,0,0.75)"}
  menu.onclose = function(player) vRP.openMainMenu(player) end

    if vRP.hasPermission(user_id, "player.store_money") then
      menu["Сложить деньги"] = {choice_store_money} -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

	if vRP.hasPermission(user_id, "player.store_money") then
      menu["Расчёска"] = {ch_fixhair} -- transforms money in wallet to money in inventory to be stored in houses and cars
    end

    if vRP.hasPermission(user_id, "player.loot") then
      menu["Обыскать и забрать"] = {choice_loot} -- take the items of nearest player in coma
    end

  vRP.openMenu(player, menu)

                  end}
        add(choices)
    end
end)
