--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]


local lang = {
  garage = {
    buy = {
	  item = "{2} {1}<br /><br />{3}",
	  request = "Вы уверены, что хотите купить эту машину?",
	},
    keys = {
	  title = "Ключи",
	  key = "Ключ ({1})",
	  description = "Проверьте ключи от своего автомобиля.",
	  sell = {
	    title = "Продать.",
		prompt = "Стоимость:",
	    owned = "Этот автомобиль уже есть у Вас.",
		request = "Вы принимаете предложение купить {1} или {2}?",
		description = "Предложить продать автомобиль ближайшему игроку."
	  }
	},
    personal = {
	  client = {
	    locked = "Машина закрыта.",
		unlocked = "Машина открыта."
	  },
	  out = "Этот автомобиль уже вне гаража.",
	  bought = "Автомобиль отправлен в ваш гараж.",
	  sold = "Автомобиль продан.",
	  stored = "Автомобиль хранится.",
	  toofar = "Машина далеко."
	},
	showroom = {
	  title = "Предпросмотр",
	  description = "Нажмите стрелочку вправо, чтобы посмотреть машину. Enter, чтобы купить."
	},
    shop = {
	  title = "Магазин",
	  description = "Прокрутите модификации автомобиля.",
	  client = {
	    nomods = "~r~Нет модификаций этого типа для этого транспортного средства.",
		maximum = "Вы достигли значения ~y~maximum~w~ для этой модификации.",
		minimum = "Вы достигли значения ~r~minimum~w~ для этой модификации.",
	    toggle = {
		  applied = "~g~Модификация применена.",
		  removed = "~r~Модификация удалена."
		}
	  },
	  mods = {
	    title = "Модификации.",
		info = "Прокрутка модификаций.",
	  },
	  repair = {
	    title = "Починить.",
		info = "Починить Ваш автомобиль.",
	  },
	  colour = {
	    title = "Цвет.",
		info = "Прокрутка цветов автомобиля.",
		primary = "Основной цвет.",
		secondary = "Вторичный цвет.",
	    extra = {
		  title = "Дополнительный цвет.",
		  info = "Прокрутка дополнительных цветов.",
	      pearlescent = "Перламутровый цвет.",
	      wheel = "Цвет колёс.",
	      smoke = "Цвет дыма.",
		},
		custom = {
		  primary = "Пользовательский основной цвет.",
		  secondary = "Пользовательский вторичный цвет.",
		},
	  },
	  neon = {
	    title = "Неоновые огни.",
		info = "Изменить неоновые огни.",
	    front = "Передний неон.",
	    back = "Задний неон.",
	    left = "Левый неон.",
	    right = "Правый неон.",
	    colour = "Цвет неона."
	  }
	}
  }
}

return lang
