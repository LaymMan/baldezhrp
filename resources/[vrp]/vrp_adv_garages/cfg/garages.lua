--[[
    FiveM Scripts
    Copyright C 2018  Sighmir

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    at your option any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
]]


local cfg = {}
-- define garage types with their associated vehicles
-- (vehicle list: https://wiki.fivem.net/wiki/Vehicles)

-- each garage type is an associated list of veh_name/veh_definition
-- they need a _config property to define the blip and the vehicle type for the garage (each vtype allow one vehicle to be spawned at a time, the default vtype is "default")
-- this is used to let the player spawn a boat AND a car at the same time for example, and only despawn it in the correct garage
-- _config: gtype, vtype, blipid, blipcolor, ghome, permissions (optional, only users with the permission will have access to the shop)
-- vtype: identifies the "type" of vehicle for the personal garages and vehicles (you can create new ones)
-- gtype: there are 5 gtypes> personal, showroom, shop, store and rental (you cant create new ones, one garage can have many gtypes)
   -- personal: allow you to get any personal vehicle of the same vtype of the garage
   -- showroom: allows you to see the vehicle model before purchasing it
   -- shop: allows you to modify your vehicle
   -- store: allows you to purchase and sell vehicles
   -- rental: allows you to rent vehicles for that session for a part of the price
-- ghome: links the garage with an address, only owners of that address will have see the garage
-- gpay: bank or wallet
-- Car/Mod: [id/model] = {"Display Name", price/amount, "", (optional) item}, -- when charging items, price becomes amount

cfg.lang = "en" -- lenguage file

cfg.rent_factor = 0.1 -- 10% of the original price if a rent
cfg.sell_factor = 0.75 -- sell for 75% of the original price

cfg.price = {
  repair = 500, -- value to repair the vehicle
  colour = 500, -- value will be charged 3 times for RGB
  extra = 300, -- value will be charged 3 times for RGB
  neon = 1500 -- value will be charged 3 times for RGB
}

-- declare any item used on purchase that doesnt exist yet (name,description,choices,weight}
cfg.items = {
  ["issi2key"] = {"Issi 2 Key","Buys an Issi",nil,0.5} -- example
}

-- configure garage types
cfg.adv_garages = {

  ["Police"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},permissions={"car.police"},vtype="police"},
    ["policeb"] = {"Мотоцикл",1500, ""},
    ["police"] = {"Машина полиции 1",3500, ""},
    ["police2"] = {"Машина полиции 2",4500, ""},
    ["police3"] = {"Машина полиции 3",5500, ""},
    ["police4"] = {"Машина полиции 4",3500, ""},
    ["fbi"] = {"Машина FBI",7000, ""}
  },
  ["Police2"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},permissions={"car.police"},vtype="police2"},
    ["pbus"] = {"Автобус",15000, ""},
    ["policet"] = {"Полицейский фургон",20000, ""},
    ["riot"] = {"Бронетехника",25000, ""}
  },
  ["Police3"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},permissions={"car.police"},vtype="police3"},
    ["polmav"] = {"Вертолёт",25000, ""}
  },
  ["Hospital"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},permissions={"emergency.service"},vtype="hospital"},
    ["ambulance"] = {"Машина EMC",5500, ""}
  },
  ["Hospital2"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},permissions={"emergency.service"},vtype="hospital2"},
    ["polmav"] = {"Вертолёт",25000, ""}
  },
  ["Hospital3"] = {
    _config = {gpay="wallet",gtype={"personal"},permissions={"emergency.service"},vtype="car"},
  },
  ["Police4"] = {
    _config = {gpay="wallet",gtype={"personal"},permissions={"car.police"},vtype="car"},
  },
  ["Taxi"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},permissions={"car.taxi"},vtype="taxi"},
    ["taxi"] = {"Такси",1000, ""},
    ["bus"] = {"Автобус",10000, ""},
    ["stretch"] = {"Лимузин",5000, ""}
  },
  ["Sedan"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="car"},
    ["Asea"] = {"Asea",7000, ""},
    ["Asterope"] = {"Asterope", 7000, ""},
    ["Cog55"] = {"Cog", 23000, ""},
    ["Cognoscenti"] = {"Cognoscenti", 17000, ""},
    ["Emperor"] = {"Emperor", 3000, ""},
    ["Emperor2"] = {"Emperor Ebay", 1200, ""},
    ["Fugitive"] = {"Fugitive", 10000, ""},
    ["Glendale"] = {"Glendale", 5000, ""},
    ["Ingot"] = {"Ingot", 4000, ""},
    ["Intruder"] = {"Intruder", 6000, ""},
    ["Premier"] = {"Premier", 5000, ""},
    ["Primo"] = {"Primo", 3000, ""},
    ["Regina"] = {"Regina", 2000, ""},
    ["Stanier"] = {"Stanier", 3000, ""},
    ["Stratum"] = {"Stratum", 3000, ""},
    ["Surge"] = {"Surge", 9000, ""},
    ["Tailgater"] = {"Tailgater", 12000, ""},
    ["Warrener"] = {"Warrener", 6000, ""},
    ["Washington"] = {"Washington", 6000, ""}
  },
  ["Compact"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="car"},
    ["Blista"] = {"BLista", 3000, ""},
    ["Blista2"] = {"Blista2", 1500, ""},
    ["Blista3"] = {"Blista3", 4000, ""},
    ["Brioso"] = {"Brioso", 4000, ""},
    ["Dilettante"] = {"Dilettante", 5000, ""},
    ["Issi2"] = {"Issi2", 3000, ""},
    ["Panto"] = {"Panto", 1000, ""},
    ["Prairie"] = {"Prairie", 4000, ""},
    ["Rhapsody"] = {"Rhapsody", 1500, ""}
  },
  ["Coupe"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="car"},
    ["CogCabrio"] = {"CogCabrio", 20000, ""},
    ["Exemplar"] = {"Exemplar", 22000, ""},
    ["F620"] = {"F620", 19000, ""},
    ["Felon"] = {"Felon", 14000, ""},
    ["Felon2"] = {"Felon2", 14500, ""},
    ["Jackal"] = {"Jackal", 17000, ""},
    ["Oracle"] = {"Oracle", 14000, ""},
    ["Oracle2"] = {"Oracle2", 16000, ""},
    ["Sentinel"] = {"Sentinel", 19000, ""},
    ["Sentinel2"] = {"Sentinel2", 20000, ""},
    ["Zion"] = {"Zion", 17000, ""},
    ["Zion2"] = {"Zion2", 18000, ""}
  },
  ["Moto"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="moto"},
    ["Akuma"] = {"Akuma", 3000, ""},
    ["Bagger"] = {"Bagger", 800, ""},
    ["Bati2"] = {"Bati2", 5700, ""},
    ["Bati"] = {"Bati", 5500, ""},
    ["BF400"] = {"BF400", 1300, ""},
    ["CarbonRS"] = {"CarbonRS", 3000, ""},
    ["Cliffhanger"] = {"Cliffhanger", 1200, ""},
    ["Daemon"] = {"Daemon", 1000, ""},
    ["Double"] = {"Double", 4800, ""},
    ["Enduro"] = {"Enduro", 800, ""},
    ["Faggio2"] = {"Faggio2", 200, ""},
    ["Fcr2"] = {"Fcr2", 2300, ""},
    ["Fcr"] = {"Fcr", 1800, ""},
    ["Gargoyle"] = {"Gargoyle", 2200, ""},
    ["Hakuchou"] = {"Hakuchou", 2500, ""},
    ["Hexer"] = {"Hexer", 2300, ""},
    ["Lectro"] = {"Lectro", 2000, ""},
    ["Nemesis"] = {"Nemesis", 2000, ""},
    ["PCJ"] = {"PCJ", 1700, ""},
    ["Ruffian"] = {"Ruffian", 1500, ""},
    ["Sanchez2"] = {"Sanchez2", 1300, ""},
    ["Sanchez"] = {"Sanchez", 1600, ""},
    ["Sovereign"] = {"Sovereign", 2300, ""},
    ["Thrust"] = {"Thrust", 3000, ""},
    ["Vader"] = {"Vader", 2100, ""}
  },
  ["Muscle"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="car"},
    ["Blade"] = {"Blade", 2300, ""},
    ["Buccaneer"] = {"Buccaneer", 1700, ""},
    ["Chino"] = {"Chino", 3600, ""},
    ["Dominator"] = {"Dominator", 7700, ""},
    ["Dominator2"] = {"Dominator2", 8000, ""},
    ["Dukes"] = {"Dukes", 2000, ""},
    ["Faction"] = {"Faction", 2100, ""},
    ["Gauntlet"] = {"Gauntlet", 5000, ""},
    ["Gauntlet2"] = {"Gauntlet2", 5500, ""},
    ["Hotknife"] = {"Hotknife", 3100, ""},
    ["Lurcher"] = {"Lurcher", 2000, ""},
    ["Moonbeam"] = {"Moonbeam", 4000, ""},
    ["Nightshade"] = {"Nightshade", 4500, ""},
    ["Phoenix"] = {"Phoenix", 3500, ""},
    ["Picador"] = {"Picador", 3200, ""},
    ["RatLoader"] = {"RatLoader", 1500, ""},
    ["Ruiner"] = {"Ruiner", 4000, ""},
    ["SabreGT"] = {"SabreGT", 4200, ""},
    ["SlamVan"] = {"SlamVan", 2400, ""},
    ["SlamVan2"] = {"SlamVan2", 2600, ""},
    ["Stalion"] = {"Stalion", 4000, ""},
    ["Stalion2"] = {"Stalion2", 4400, ""},
    ["Tampa"] = {"Tampa", 5000, ""},
    ["Vigero"] = {"Vigero", 3200, ""},
    ["Virgo"] = {"Virgo", 3000, ""},
    ["Voodoo"] = {"Voodoo", 3000, ""},
    ["Voodoo2"] = {"Voodoo2", 1500, ""}
  },
  ["Off-road"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="car"},
    ["BfInjection"] = {"BfInjection", 1800, ""},
    ["Bodhi2"] = {"Bodhi2", 2500, ""},
    ["Brawler"] = {"Brawler", 4000, ""},
    ["DLoader"] = {"DLoader", 1200, ""},
    ["Mesa"] = {"Mesa", 3000, ""},
    ["Mesa3"] = {"Mesa3", 4000, ""},
    ["RancherXL"] = {"RancherXL", 2700, ""},
    ["Rebel"] = {"Rebel", 3300, ""},
    ["Rebel2"] = {"Rebel2", 3100, ""},
    ["Sandking"] = {"Sandking", 8700, ""}
  },
  ["SUV"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="car"},
    ["BJXL"] = {"BJXL", 8000, ""},
    ["Baller"] = {"Baller", 11000, ""},
    ["Baller2"] = {"Baller2", 14000, ""},
    ["Cavalcade"] = {"Cavalcade", 12000, ""},
    ["Cavalcade2"] = {"Cavalcade2", 13500, ""},
    ["Dubsta"] = {"Dubsta", 15000, ""},
    ["Dubsta3"] = {"Dubsta3", 20000, ""},
    ["FQ2"] = {"FQ2", 12000, ""},
    ["Granger"] = {"Granger", 21000, ""},
    ["Gresley"] = {"Gresley", 15000, ""},
    ["Habanero"] = {"Habanero", 10000, ""},
    ["Huntley"] = {"Huntley", 11000, ""},
    ["Landstalker"] = {"Landstalker", 11700, ""},
    ["Patriot"] = {"Patriot", 14000, ""},
    ["Radi"] = {"Radi", 11300, ""},
    ["Rocoto"] = {"Rocoto", 14000, ""},
    ["Seminole"] = {"Seminole", 9200, ""},
    ["Serrano"] = {"Serrano", 10300, ""},
    ["XLS"] = {"XLS", 13000, ""}
  },
  ["Sport"] = {
    _config = {gpay="wallet",gtype={"showroom","store","personal"},vtype="car"},
    ["Alpha"] = {"Alpha", 22000, ""},
    ["Banshee"] = {"Banshee", 26000, ""},
    ["BestiaGTS"] = {"BestiaGTS", 29000, ""}
  },

  --["Ranch Main"]  = {
    --_config = {gpay="wallet",gtype={"personal"},vtype="car",blipid=357,blipcolor=3,ghome="Ranch Main"},
--  },

["LS Customs"]  = {
  _config = {gpay="wallet",gtype={"shop"},vtype="car",blipid=72,blipcolor=7},
_shop = {
  -- You can make different shops with different modifications for each garage of gtype shop
  [0] = {"Spoilers",500,""},
  [1] = {"Front Bumper",500,""},
    [2] = {"Rear Bumper",500,""},
    [3] = {"Side Skirt",500,""},
    [4] = {"Exhaust",500,""},
    [5] = {"Frame",500,""},
    [6] = {"Grille",500,""},
    [7] = {"Hood",500,""},
    [8] = {"Fender",500,""},
    [9] = {"Right Fender",500,""},
    [10] = {"Roof",500,""},
    [11] = {"Engine",500,""},
    [12] = {"Brakes",500,""},
    [13] = {"Transmission",500,""},
    [14] = {"Horns",500,""},
    [15] = {"Suspension",500,""},
    [16] = {"Armor",500,""},
    [18] = {"Turbo",500,""},
    [20] = {"Tire Smoke",500,""},
    [22] = {"Xenon Headlights",500,""},
    [23] = {"Wheels",500,"Press enter to change wheel type"},
    [24] = {"Back Wheels (Bike)",500,""},
    [25] = {"Plateholders",500,""},
    [27] = {"Trims",500,""},
    [28] = {"Ornaments",500,""},
    [29] = {"Dashboards",500,""},
    [30] = {"Dials",500,""},
    [31] = {"Door Speakers",500,""},
    [32] = {"Seats",500,""},
    [33] = {"Steering Wheel",500,""},
    [34] = {"H Shift",500,""},
    [35] = {"Plates",500,""},
    [36] = {"Speakers",500,""},
    [37] = {"Trunks",500,""},
    [38] = {"Hydraulics",500,""},
    [39] = {"Engine Block",500,""},
    [40] = {"Air Filter",500,""},
    [41] = {"Struts",500,""},
    [42] = {"Arch Covers",500,""},
    [43] = {"Arials",500,""},
    [44] = {"Extra Trims",500,""},
    [45] = {"Tanks",500,""},
    [46] = {"Windows",500,""},
    [48] = {"Livery",500,""},
}
},
}

-- position garages on the map {garage_type,x,y,z}
cfg.garages = {

  -- default garages
  {"Police",462.74365234375,-1019.4291381836,28.103860855102},
  {"Police",462.74365234375,-1014.8120117188,28.06887435913},
  {"Police2",477.65634155274,-1022.8350219726,28.043210983276},
  {"Police3",449.57946777344,-981.17083740234,43.691635131836},
  {"Police4",452.46755981446,-997.40405273438,25.757577896118},
  {"Police4",447.37561035156,-997.44018554688,25.757642745972},
  {"Hospital",-466.62557983398,-323.0244140625,34.36381149292},
  {"Hospital2",-456.51889038086,-291.30856323242,78.193466186524},
  {"Hospital3",-460.71755981446,-273.05819702148,35.777645111084},
  {"Taxi",916.78900146484,-162.8897857666,74.717247009278},


  -- personal garages
  {"Sedan",-146.85145568848,-1164.984741211,25.284128189086},
  {"Compact",60.693195343018,17.996227264404,69.233436584472},
  {"Coupe",-337.0863647461,-1077.8572998046,23.025791168212},
  {"Moto",1097.9836425782,-260.58367919922,69.244270324708},
  {"Muscle",-180.71466064454,-1376.6497802734,31.258222579956},
  {"Off-road",1909.2645263672,3730.3110351562,32.584396362304},
  {"SUV",-149.58209228516,6358.5747070312,31.490608215332},


  -- lscustoms
  {"LS Customs",-337.3863,-136.9247,39.0737},
  {"LS Customs",-1155.536,-2007.183,13.244},
  {"LS Customs",731.8163,-1088.822,22.233},
  {"LS Customs",1175.04,2640.216,37.82177},
  {"LS Customs",110.8406,6626.568,32.287},

  -- house garages
  --{"Ranch Main", 1408.32495117188,1117.44665527344,114.737692260742},


  -- planes and boats

  --{"planes",2123, 4805, 41.19},
  --{"helicopters",-745, -1468, 5},
  --{"boats",1538, 3902, 30.35}

}

return cfg
